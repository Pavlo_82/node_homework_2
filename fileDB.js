// imports
const fs = require('fs')
// const fsp = require('fs/promises')
const path = require('path')
const nanoid = require('nanoid')
// errors
const errors = {
  notFound: {
    statusCode: 404,
    message: 'could not be found'
  },
  fileProblems: {
    statusCode: 500,
    message: 'cannot create file or directory'
  },
  field: {
    statusCode: 404,
    message: 'no field text or title'
  },
  database: {
    statusCode: 404,
    message: 'no database'
  }
}

// helper find elem
const findEl = (data, id) => {
  return data.find((item) => item.id === id)
}

// validation
const validate = ({ text, title }) => {
  return { text, title }
}

const schemas = {}

const registerSchema = (schemaName, schema) => {
  schemas[schemaName] = schema
}

const initDB = async (file, folder) => {
  try {
    await fs.promises.mkdir(folder, { recursive: true })
    await fs.promises.writeFile(file, JSON.stringify([]))
  } catch (err) {
    console.error('Error initializing database:', err)
  }
}

// function getTable return CRUD
const getTable = async (database) => {
  // check table
  const schema = schemas[database]

  if (!schema) {
    throw new Error(errors.database)
  }

  // check if folder or create
  const folder = path.join(__dirname, 'db')
  const file = path.join(folder, `${database}.json`)
  const exists = await fs.existsSync(folder)

  if (!exists) {
    await initDB(file, folder)
  }

  // read
  const readData = async (file) => {
    if (!file) {
      console.error('File path not provided')
      return
    }

    const res = await fs.promises.readFile(file, 'utf8')
    const data = await JSON.parse(res)
    return data
  }
  // write
  const writeData = async (data) => {
    const jsonData = JSON.stringify(data)
    return fs.promises.writeFile(file, jsonData)
  }
  return {
    // create
    async create (data) {
      try {
        const { title, text } = validate(data)
        if (!title || !text) {
          throw new Error('no title or text')
        }
        const id = nanoid.nanoid()
        const createDate = new Date()
        const formData = {
          id,
          title,
          text,
          createDate,
          updateDate: createDate
        }

        const all = await readData(file)
        all.push(formData)
        await writeData(all)

        return formData
      } catch (err) {
        console.error('Error in create:', err)
        return null
      }
    },
    // update
    async update (data, id) {
      try {
        const { title, text } = validate(data)
        const all = await readData(file) // Here we await the result
        const updateDate = new Date()

        const found = await findEl(all, id) // Using all, which is now a resolved value

        if (!found) {
          throw new Error('not found')
        }

        const updatedText = text || found.text
        const updatedTitle = title || found.title
        const updated = {
          id: found.id,
          text: updatedText,
          title: updatedTitle,
          createDate: found.createDate,
          updateDate
        }

        const newData = all.map((item) => (item.id === id ? updated : item)) // Again, using all
        await writeData(newData) // Await here as well
      } catch (err) {
        console.error('Error in update:', err)
        return null
      }
    },
    // get by id
    async getById (id) {
      try {
        console.log(id)
        const all = await readData(file)
        const found = await findEl(all, id) // Using all, which is now a resolved value

        if (!found) {
          throw new Error('not found')
        }
        return found
      } catch (err) {
        console.error('Error in update:', err)
        return null
      }
    },
    // get all
    async getAll () {
      try {
        const data = await readData(file)
        return data
      } catch (err) {
        console.error('Error in update:', err)
        return null
      }
    },
    // delete
    async delete (id) {
      try {
        const all = await readData(file)
        const found = await findEl(all, id) // Using all, which is now a resolved value

        if (!found) {
          throw new Error('not found')
        }
        const deleted = all.filter((item) => item.id !== id)
        writeData(deleted)
        return {
          message: { message: 'succesfull deleted', id }
        }
      } catch (err) {
        console.error('Error in update:', err)
        return null
      }
    }
  }
}

module.exports = {
  getTable,
  registerSchema
}
