// imports
const fileDB = require('./fileDB')

// database var
const dbName = 'db'

// schema
const newspostSchema = {
  id: String,
  title: String,
  text: String,
  createDate: Date,
  updateDate: Date
}
const start = async (newspostSchema) => {
  await fileDB.registerSchema(dbName, newspostSchema)
  const newspostTable = await fileDB.getTable(dbName)

  // get by id
  const findById = 'cMh87Pb82DXpwt2HwD5Rk'

  const one = await newspostTable.getById(findById)
  console.log(one, 'getById')

  // get all
  const data = await newspostTable.getAll()
  console.log(data, 'get all data')

  // data for create
  const newPost = {
    title: 'Something',
    text: 'Something'
  }

  // create data
  const created = await newspostTable.create(newPost)
  console.log(created, 'created')

  // data for update
  const updateData = {
    title: 'Anything gfd',
    text: 'Anything gfd'
  }

  // update data
  const idUpdate = '0lELZzO3tq0ow7-2sMDlo'
  const updated = await newspostTable.update(updateData, idUpdate)
  console.log(updated, 'updated')

  // delete
  const deletedId = '6sXpwEOpvBdRfztws7tlS'
  const message = await newspostTable.delete(deletedId)
  console.log(message, 'deleted')
}

start(newspostSchema)
